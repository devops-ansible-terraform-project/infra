#!/bin/bash

GOGOL=$(echo $(terraform output gogol_public_ip) | tr -d '"')
RUNNER=$(echo $(terraform output runner_public_ip) | tr -d '"')
MONITORING=$(echo $(terraform output monitoring_public_ip) | tr -d '"')

printf "[base]\n$GOGOL\n$RUNNER\n$MONITORING\n[runner]\n$RUNNER\n[monitoring]\n$MONITORING\n[node_exporter]\n$GOGOL\n" > ./ansible/inv.inv
