# Infrastructure

This repository is part of the DevOps Ansible + Terraform project.

The code prepares the infrastructure for services in `AWS` using `Terraform` and configures hosts using `Ansible`.

- [Components of the project](#components-of-the-project)
  - [Services](#services)
  - [Recources](#recources)
- [Configure the GitLab Repository](#configure-the-gitlab-repository)
- [Prepare the ENV vars for local development](#prepare-the-env-vars-for-local-development)
- [Terraform](#terraform)
  - [Init](#init)
  - [Plan](#plan)
  - [Apply](#apply)
- [Ansible](#ansible)
  - [Base](#base)
  - [Runner](#runner)
  - [Monitoring](#monitoring)
  - [Node Exporter](#node-exporter)
- [Grafana Dashboards](#grafana-dashboards)

# Components of the project

The infrastructure separates `test` and `prod` using the corresponding `develop` and `master` branches.

Set `develop` as the default branch.

### Services

The infrastructure has the following services for `test` and `prod` environments:

- one microservice – `Gogol`\* (see [devops-ansible-terraform-project/service](https://gitlab.com/devops-ansible-terraform-project/service))
- the monitoring stack:
  - `Prometheus`
  - `Promtail`
  - `Grafana`
  - `Loki`
  - `Node Exporter`

\*The `Gogol` service is launched separately using `GitOps`.

### Resources

`Terraform` creates the following resources after applying the plan:

- `ACM`
- `ALB`
- `EC2`
- `VPC`
- `Route53`
- `Security Groups`
- and some auxiliary recources (see the configuration for a more detailed understanding)

For `test` and `prod`, resources are created separately for the specified backend, which is stored in `Gitlab`.

# Configure the GitLab Repository

All manual steps that will be described in the [Terraform](#Terraform) section will be automatically reproduced in `GitLab CI/CD`, thus, it is necessary to specify the following values in the variables for the runner:

- `GITLAB_TOKEN` – to access `TF_STATE` file
- `AWS_ACCESS_KEY_ID` – to access `AWS` account
- `AWS_SECRET_ACCESS_KEY` – to access `AWS` account

# Prepare the ENV vars for local development

There are two `.env` files for `Ansible` and `Terraform` respectively. These files are used in the `Makefile` to speed up development using the `make` commands.

- The contents of the `.env` file in the terraform directory `/infra/.env` should be as follows:

  ```bash
  ENV          = $INFRA_ENV         # `test` or `prod` value
  GITLAB_TOKEN = $YOUR_GITLAB_TOKEN # `your token` from GitLab
  ```

  The `ENV` can be `test` or `prod`

- The contents of the `.env` file in the ansible `/infra/ansible/.env` directory should be as follows:

  ```bash
  ENV             = $INFRA_ENV                        # `test` or `prod` value
  DOMAIN          = $YOUR_DOMAIN                      # `your.domain` value
  URL_ENV         = $SUFFIX_IN_URL                    # `test.` or `` value
  URL_SERVICE     = ${URL_ENV}${DOMAIN}               # `test.your.domain` or `your.domain` value
  URL_SERVER_NODE = ${SERVER_NODE_SUBDOMAIN}${DOMAIN} # `server-node-test.your.domain` or `server-node.your.domain` value
  ```

  The `DOMAIN` in this project is `staboss.com `, but can be changed in the `terraform` config files.

# Terraform

### Init

```bash
make reinit
```

### Plan

```bash
make plan
```

### Apply

```bash
make apply
```

# Ansible

Before launching the playbooks, run the following `make` command in the source directory, and then go to the ansible directory:

```bash
make inventory
cd ansible/
```

It is necessary to create an `ansible_vault_pass` file with a password to run the task for GitLab runner.

### Base

Basic configuration of hosts.

```bash
make base
```

### Runner

Configuration of Gitlab runners.

```bash
make runner
```

Then you need to connect to the host:

```bash
ssh $REMOTE_USER@$REMOTE_HOST -i $PRIVATE_KEY_FILE
```

Get the ID of the runner container and perform some operations inside the container:

```bash
sudo docker exec -it $RUNNER_CONTAINER_ID /bin/bash
```

```bash
apt-get update
apt-get install nano
nano /etc/gitlab-runner/config.toml
   -> privileged = true
```

```bash
sudo docker restart $RUNNER_CONTAINER_ID
```

### Monitoring

```bash
make monitoring
```

### Node Exporter

```bash
make node_exporter
```

# Grafana Dashboards

```
Go Service Dashboard ID = 14061
Node State Dashboard ID = 1860
```
