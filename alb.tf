locals {
  targets = [
    {
      name             = "gogol-${var.environment}"
      backend_port     = 8080
      backend_protocol = "HTTP"
      target_type      = "instance"
      target_id        = module.gogol.id
      host_header      = ["${var.subdomain}${var.domain}"]

      health_check = {
        enabled  = true
        port     = 8080
        protocol = "HTTP"
        path     = "/health"
      }
    },
    {
      name             = "grafana-${var.environment}"
      backend_port     = 3000
      backend_protocol = "HTTP"
      target_type      = "instance"
      target_id        = module.monitoring.id
      host_header      = ["${var.grafana_subdomain}${var.domain}"]

      health_check = {
        enabled  = true
        port     = 3000
        protocol = "HTTP"
        path     = "/"
      }
    },
    {
      name             = "monitoring-${var.environment}"
      backend_port     = 9090
      backend_protocol = "HTTP"
      target_type      = "instance"
      target_id        = module.monitoring.id
      host_header      = ["${var.monitoring_subdomain}${var.domain}"]

      health_check = {
        enabled  = true
        port     = 9090
        protocol = "HTTP"
        path     = "/"
      }
    },
    {
      name             = "server-node-metrics-${var.environment}"
      backend_port     = 9100
      backend_protocol = "HTTP"
      target_type      = "instance"
      target_id        = module.gogol.id
      host_header      = ["${var.server_node_subdomain}${var.domain}"]

      health_check = {
        enabled  = true
        port     = 9100
        protocol = "HTTP"
        path     = "/"
      }
    },
  ]
}

module "alb" {
  source = "./modules/aws/lb"
  name   = "alb-${var.environment}"

  create_lb = var.create_lb
  subnets   = module.vpc.public_subnets
  tags      = var.tags
  vpc_id    = module.vpc.vpc_id

  certificate_arn = module.acm.acm_certificate_arn
  security_groups = [module.web_security_group.security_group_id]

  target_groups = local.targets
}
